package com.sheesha.android.fragments;

import java.util.Arrays;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.widget.LoginButton;
import com.sheesha.android.R;

public class FacebookLoginFragment extends android.support.v4.app.Fragment {
	private static final String TAG = "FacebookLoginFragment";
	private UiLifecycleHelper uiHelper;
	Session mSession;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.activity_login,
				container, false);
		LoginButton btnFbLogin = (LoginButton) view
				.findViewById(R.id.login_FBLogin);
		btnFbLogin.setFragment(this);
		btnFbLogin.setReadPermissions(Arrays.asList("basic_info"));
		Log.d(TAG, "Getting Basic Info from facebook");
		return view;
	}
	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		Log.d(TAG, "Recieved at SessionStateChage from facebook, Session state is:"+state.toString());
		
		if (state.isOpened()) {
			Log.d(TAG, "Session State Opened from facebook");
			if (mSession == null || isSessionChanged(session)) {
				mSession = session;
				Log.d(TAG, "Logged in using facebook");
				Log.d(TAG, "Access Token: " + session.getAccessToken());
				//processResponse(session.getAccessToken());
			}
		} else if (state.isClosed()) {
			Log.d(TAG, "Logged out from facebook");
		}
	}

	private boolean isSessionChanged(Session session) {

		// Check if session state changed
		if (mSession.getState() != session.getState())
		{
			return true;
		}
		// Check if accessToken changed
		if (mSession.getAccessToken() != null) 
		{
			if (!mSession.getAccessToken().equals(session.getAccessToken()))
				{
					return true;
				}
		} else if (session.getAccessToken() != null) 
		{
			return true;
		}
		// Nothing changed
		return false;
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}
		uiHelper.onResume();
	}
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}
//	private void processResponse(String access_token) {
//		JSONObject jsonobject = new JSONObject();
//		String LoginUrl = GlobalConstants.API_FACEBOOK_CONNECT
//				+ "?access_token=" + access_token;
//
//		Listener<JSONObject> response = new Listener<JSONObject>() {
//
//			private String tokenString;
//			private String emailString;
//			private String nameString;
//
//			@Override
//			public void onResponse(JSONObject response) {
//				try {
//					emailString = response.getString("email");
//					tokenString = response.getString("password");
//					nameString = response.getString("name");
//				} catch (JSONException e) {
//					e.printStackTrace();
//				}
//				((BlugaaApplication) getActivity().getApplication())
//						.setCredentials(nameString, emailString, tokenString);
//				Intent intent = new Intent(getActivity(), HomeActivity.class);
//				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
//						| Intent.FLAG_ACTIVITY_CLEAR_TASK);
//				startActivity(intent);
//				getActivity().finish();
//			}
//		};
//		ErrorListener error = new ErrorListener() {
//			@Override
//			public void onErrorResponse(VolleyError error) {
//				Toast.makeText(getActivity(),
//						"Login failed.\nPlease try again later.",
//						Toast.LENGTH_SHORT).show();
//
//			}
//		};
//		JsonObjectRequest jsonobjreq = new JsonObjectRequest(
//				com.android.volley.Request.Method.GET, LoginUrl, jsonobject,
//				response, error);
//		jsonobjreq.setShouldCache(false);
//		VolleySingleton.getInstance(getActivity()).getmRequestQueue()
//				.getCache().clear();
//		VolleySingleton.getInstance(getActivity()).getmRequestQueue()
//				.add(jsonobjreq);
//
//	}
}
