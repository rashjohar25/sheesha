package com.sheesha.android;

import java.util.ArrayList;
import java.util.List;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.sheesha.android.fragments.FacebookLoginFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

public class LoginActivity extends FragmentActivity{
	private FacebookLoginFragment mainFragment;
	private Intent intent;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		List<String> permissions = new ArrayList<String>();
		permissions.add("email");
		if (Session.getActiveSession() != null) {
			Session.openActiveSession(this, true, permissions,
					new Session.StatusCallback() {
						@Override
						public void call(Session session, SessionState state,
								Exception exception) {
							if (session.isOpened()) {
								Request.newMeRequest(session,
										new Request.GraphUserCallback() {

											@Override
											public void onCompleted(
													GraphUser user,
													Response response) {
												if (user != null) {
													
												}
											}
										}).executeAsync();
							}
						}
					});
		} else {
			mainFragment = new FacebookLoginFragment();
			getSupportFragmentManager().beginTransaction()
					.add(android.R.id.content, mainFragment).commit();
		}
	}
}
